HelloFresh Dev APP
==================

Installation
------------

### Vagrant&VirtualBox

1. ›`git clone git@github.com:thewulf7/hellofresh.git`

2. Install Vagrant(https://www.vagrantup.com/) and VirtualBox(https://www.virtualbox.org/)

3. ›`vagrant up`

### Composer

4. ›`vagrant ssh`

5. ›`cd /var/www/project && ./run.sh`

### Use

6. Web interface - http://app.dev:8090

7. REST interface - http://api.app.dev:8080/v1/

Solution
--------

* REST service with AngularJS web interface.
* Abstract Page Data Pattern for pages.
* DI for config.
* AbstractFactory for routing and validation.


