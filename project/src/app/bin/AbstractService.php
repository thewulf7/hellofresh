<?php
namespace hellofresh\bin;


/**
 * Class AbstractService
 *
 * @package hellofresh\bin
 */
class AbstractService extends ServiceInjector
{
    protected $className;

    public function findModel($id) {
        $className = $this->className;
        $model = $className::findOrFail($id);

        return $model;
    }
}