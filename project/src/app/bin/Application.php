<?php
namespace hellofresh\bin;


use fkooman\Rest\Service;
use hellofresh\bin\Route\RouteRuleFactory;
use hellofresh\config\iConfig;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
 * Class Application
 *
 * @package hellofresh\app\Bin
 */
class Application extends ServiceInjector
{
    public static $service;
    public static $config;

    public static $db;

    const API_VERSION = 'v1';

    /**
     * @param iConfig $config
     * @param Service $service
     */
    public function __construct(iConfig $config, Service $service)
    {
        session_start();
        self::$config  = $config;
        self::$service = $service;

        $db = $config->get('db');

        self::$db = new Capsule;

        self::$db->addConnection([
                                     'driver'    => 'mysql',
                                     'host'      => $db['dbhost'],
                                     'database'  => $db['dbname'],
                                     'username'  => $db['dbuser'],
                                     'password'  => $db['dbpass'],
                                     'charset'   => 'utf8',
                                     'collation' => 'utf8_unicode_ci',
                                     'prefix'    => '',
                                 ]);

        self::$db->bootEloquent();
    }

    /**
     * Register routes for app and run it
     *
     */
    public function run()
    {
        $this->registerRoutes();
        self::$service->run()->send();
    }

    protected function registerRoutes()
    {
        $rest = self::$config->get('rest');

        $ruleFactory = new RouteRuleFactory();


        foreach ($rest as $route => $controller) {
            $rule = $ruleFactory->create($route, $controller);
            $rule->register();

        }

    }

    /**
     * @param $password
     * @param $salt
     *
     * @return string
     */
    public static function generatePasswd($password, $salt)
    {
        $hashpasswd = crypt($password, $salt);

        return $hashpasswd;
    }

    /**
     * @return string
     */
    public static function generateSalt()
    {
        $salt = '$2a$10$' . substr(str_replace('+', '.', base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(), mt_rand()))), 0, 22) . '$';

        return $salt;
    }

    public static function setAuth($hash)
    {
        setcookie('AUTH_KEY', $hash, time() + 60 * 60 * 24 * 30, "/");
        $_SESSION['SESSION_KEY'] = $hash;
    }

    public static function clearAuth()
    {
        unset($_COOKIE['AUTH_KEY'],$_SESSION['SESSION_KEY']);
    }

    /**
     * @return bool
     */
    public static function isGuest()
    {
        if(array_key_exists('SESSION_KEY',$_SESSION)) {
            $hash = $_SESSION['SESSION_KEY'];
            $model = \hellofresh\models\backend\User::where('userhash',$hash)->first();
            if($model) {
                return $model;
            }
        }

        if (array_key_exists('HTTP_AUTH_KEY', $_SERVER)) {
            $hash = $_SERVER['HTTP_AUTH_KEY'];
            $model = \hellofresh\models\backend\User::where('userhash',$hash)->first();
            if($model) {
                $_SESSION['SESSION_KEY'] = $hash;
                return $model;
            }
        }

        return true;
    }

}