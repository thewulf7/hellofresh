<?php
namespace hellofresh\bin;


use fkooman\Http\Exception\BadRequestException;
use fkooman\Http\JsonResponse;
use hellofresh\services\AuthService;
use hellofresh\services\EmailService;
use hellofresh\services\UserService;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Class Controller
 *
 * @method UserService  getUserService
 * @method AuthService  getAuthService
 *
 * @package hellofresh\bin
 */
abstract class Controller extends ServiceInjector
{
    /**
     * @return array
     */
    public function guestAllowed(){
        return array();
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return JsonResponse|mixed
     * @throws BadRequestException
     */
    public function __call($name, $arguments)
    {
        try {
            $action = parent::__call($name, $arguments);
            return $action;
        } catch (BadRequestException $e) {
            $method = 'action' . ucfirst($name);

            $guestAllowed = $this->guestAllowed();
            
            if (method_exists($this, $method)) {
                if(Application::isGuest() === true && !in_array($method,$guestAllowed,true)) {
                    header('HTTP/1.0 403 Forbidden');
                    return $this->sendErrorData(['Permission denied']);
                }
                return call_user_func_array(array($this,$method), $arguments);
            }

            throw new BadRequestException('Calling unknown method: ' . get_class($this) . "::$name()", 500);
        }
    }

}