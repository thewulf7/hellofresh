<?php
namespace hellofresh\bin;


use hellofresh\bin\Validation\ValidationRulefactory;

/**
 * Class Model
 *
 * @package hellofresh\bin
 */
abstract class Model
{
    protected $container = [];

    const SERIALIZE_OPTION_WITHOUT = 'without';

    /**
     * @param array $options
     *
     * @return mixed
     */
    abstract public function serialize($options=[]);

    /**
     * @return string
     */
    public function modelType()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function getAttributes()
    {

        $attributes = $this->container->toArray();
        $rulesAr    = $this->container->rules() ?: [];
        $excludeParams = [];

        $ruleFactory = new ValidationRulefactory();

        foreach ($rulesAr as $rule) {
            $rule = $ruleFactory->create($rule);
            if ($rule->getType() === ValidationRulefactory::VALIDATION_TYPE_DISABLED) {
                $excludeParams = array_merge($excludeParams,$rule->getAttribs());
            }
        }

        unset($attributes['id']);

        foreach ($attributes as $k => $v) {
            if (in_array($k, $excludeParams, true)) {
                unset($attributes[$k]);
            }
        }

        return $attributes;
    }
}