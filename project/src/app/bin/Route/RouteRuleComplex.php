<?php
namespace hellofresh\bin\Route;


use fkooman\Http\Request;
use hellofresh\bin\Application;
use hellofresh\bin\entity\iRouteRule;

/**
 * Class RouteRuleComplex
 *
 * @package hellofresh\bin
 */
class RouteRuleComplex implements iRouteRule
{
    protected $class;
    protected $actions;
    protected $route;

    public $pluralize = true;
    public $rules     = [];

    const GET    = 'get';
    const POST   = 'post';
    const PUT    = 'put';
    const DELETE = 'delete';

    /**
     * @param       $rule
     * @param array $controller
     *
     * @throws \ErrorException
     */
    public function __construct($rule, array $controller)
    {
        $cName = $controller['class'];
        try {
            $this->class = new $cName();
        } catch (\Exception $e) {
            throw new \ErrorException('Class ' . $cName . '  no exist!');
        }
        if (array_key_exists('pluralize', $controller)) {
            $this->pluralize = $controller['pluralize'];
        }

        $route = '/' . Application::API_VERSION . '/' . $rule;

        if ($this->pluralize) {
            $route .= 's';
        }
        if (array_key_exists('rules', $controller)) {
            $this->rules = $controller['rules'];
        }

        $this->route   = $route;
        $this->actions = get_class_methods($this->class->className());
    }

    /**
     * @return array
     */
    public function getAvailableActions()
    {
        $actions = [];

        foreach ($this->actions as $action) {
            if (0 === strpos($action, 'action')) {
                $actions[] = $action;
            }
        }

        return $actions;
    }

    public function register()
    {
        $actions = $this->getAvailableActions();
        $class   = $this->class;
        /*
         * TODO: Add func arguments
         * */
        foreach ($actions as $action) {
            $action     = str_replace('action', '', $action);
            $actionName = strtolower(str_replace('action', '', $action));
            if (count($this->rules) > 0) {

                $method = isset($this->rules[$actionName]) ? strtolower($this->rules[$actionName]) : null;

                if (!$method) {
                    continue;
                }

                if (in_array($method, [self::POST, self::DELETE],true)) {
                    Application::$service->$method(
                        $this->route . '/' . $actionName,
                        function (Request $request) use ($class, $action) {
                            return $class->$action($request);
                        }
                    );
                } else {
                    Application::$service->$method(
                        $this->route . '/' . $actionName,
                        function () use ($class, $action) {
                            return $class->$action();
                        }
                    );
                }
            } else {
                Application::$service->get(
                    $this->route . '/' . $actionName,
                    function () use ($class, $action) {
                        return $class->$action();
                    }
                );
            }
        }
    }
}