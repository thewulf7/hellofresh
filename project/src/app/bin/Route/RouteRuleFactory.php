<?php
namespace hellofresh\bin\Route;


use hellofresh\bin\patterns\MethodFactory;

/**
 * Class RouteRuleFactory
 *
 * @package hellofresh\bin\Route
 */
class RouteRuleFactory extends MethodFactory
{
    const ACTION_VIEW   = 'actionView';
    const ACTION_CREATE = 'actionCreate';
    const ACTION_UPDATE = 'actionUpdate';
    const ACTION_DELETE = 'actionDelete';

    /**
     * @param $arRule
     *
     * @return mixed
     * @internal param $rule
     * @internal param $controller
     *
     */
    protected function createRule($arRule)
    {
        list($rule, $controller) = $arRule;
        switch ($controller) {
            case is_array($controller):
                $routeRule = new RouteRuleComplex($rule, $controller);

                return $routeRule;
                break;
            case is_string($controller):
                $routeRule = new RouteRuleSimple($rule, $controller);

                return $routeRule;
                break;
            default:
                throw new \InvalidArgumentException("$rule is not a valid");
        }
    }

    /**
     * @param $rule
     * @param $controller
     *
     * @return RouteRuleComplex|RouteRuleSimple|mixed
     */
    public function create($rule, $controller)
    {
        $obj = $this->createRule(array($rule, $controller));

        return $obj;
    }
}