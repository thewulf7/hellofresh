<?php
namespace hellofresh\bin\Route;


use fkooman\Http\Request;
use hellofresh\bin\Application;
use hellofresh\bin\entity\iRouteRule;

/**
 * Class RouteRuleSimple
 *
 * @package hellofresh\bin
 */
class RouteRuleSimple implements iRouteRule
{
    protected $class;
    protected $actions;
    protected $route;

    /**
     * @param $rule
     * @param $controller
     *
     * @throws \ErrorException
     */
    public function __construct($rule, $controller)
    {
        try {
            $this->class = new $controller();
        } catch (\Exception $e) {
            throw new \ErrorException('Class ' . $controller . '  not exist!');
        }
        $this->route   = '/' . Application::API_VERSION . '/' . $rule . 's';
        $this->actions = get_class_methods($this->class->className());
    }

    /**
     * @return array
     */
    public function getAvailableActions()
    {
        $actions = [];

        foreach ($this->actions as $action) {
            if (0 === strpos($action, 'action')) {
                $actions[] = $action;
            }
        }

        return $actions;
    }

    /**
     * Register classes for rest
     */
    public function register()
    {
        $actions = $this->getAvailableActions();
        $class   = $this->class;

        foreach ($actions as $action) {
            switch ($action) {
                case RouteRuleFactory::ACTION_CREATE:
                    Application::$service->post(
                        $this->route,
                        function (Request $request) use ($class, $action) {
                            return $class->$action($request);
                        }
                    );
                    break;
                case RouteRuleFactory::ACTION_VIEW:
                    Application::$service->get(
                        $this->route . '/:id',
                        function ($id) use ($class, $action) {
                            return $class->$action($id);
                        }
                    );
                    break;
                case RouteRuleFactory::ACTION_UPDATE:
                    Application::$service->put(
                        $this->route . '/:id',
                        function ($id, Request $request) use ($class, $action) {
                            return $class->$action($id, $request);
                        }
                    );
                    break;
                case RouteRuleFactory::ACTION_DELETE:
                    Application::$service->delete(
                        $this->route . '/:id',
                        function ($id, Request $request) use ($class, $action) {
                            return $class->$action($id, $request);
                        }
                    );
                    break;
                default:
                    $routeParam = '';
//                    $rClass = new \ReflectionClass(get_class($class));
//                    $method = $rClass->getMethod($action);
                    $routeParam .= strtolower(str_replace('action','',$action));

                    Application::$service->post(
                        $this->route . '/' . $routeParam . '/:string',
                        function ($string) use ($routeParam,$class) {
                            return $class->$routeParam($string);
                        }
                    );
                    break;
            }
        }
    }
}