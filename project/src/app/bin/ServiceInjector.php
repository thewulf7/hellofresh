<?php
namespace hellofresh\bin;


use fkooman\Http\Exception\BadRequestException;
use fkooman\Http\JsonResponse;
use hellofresh\services\AuthService;
use hellofresh\services\EmailService;
use hellofresh\services\UserService;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Class Controller
 *
 * @method UserService  getUserService
 * @method AuthService  getAuthService
 *
 * @package hellofresh\bin
 */
class ServiceInjector
{

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     * @throws BadRequestException
     */
    public function __call($name, $arguments)
    {
        $serviceClassName = $this->getService($name);

        if (class_exists($serviceClassName)) {
            return new $serviceClassName();
        }

        throw new BadRequestException('Calling unknown method: ' . get_class($this) . "::$name()", 500);
    }

    /**
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * @param       $data
     * @param int   $code
     *
     * @return JsonResponse
     */
    public function sendData($data, $code = 200, $options=[])
    {
        $response = new JsonResponse();
        $response->codeToReason($code);

        $dataAr = [];

        if ($data instanceof Model) {
            $data = $data->serialize();
        } elseif (is_array($data)){
            foreach($data as $elem) {
                if($elem instanceof Model) {
                    $dataAr[] = $elem->serialize($options);
                }
            }
        }

        $response->setBody(
            [
                'data' => count($dataAr)>0 ? $dataAr : $data,
                'code' => $code,
            ]
        );

        return $response;
    }

    /**
     * @param array $data
     * @param int   $code
     *
     * @return JsonResponse
     */
    public function sendErrorData($data = [], $code = 403)
    {
        $response = new JsonResponse();
        $response->codeToReason($code);
        header('HTTP/1.0 ' . $code);
        $response->setBody(
            [
                'data' => [
                    'errors' => $data,
                    'code'   => $code,
                ],
            ]
        );

        return $response;
    }

    /**
     * @param int $code
     *
     * @return JsonResponse
     */
    public function sendEmptyData($code = 201)
    {
        $response = new JsonResponse();
        $response->codeToReason($code);

        return $response;
    }

    /**
     * @param $name
     *
     * @return null|string
     */
    public function getService($name)
    {
        $name      = str_replace('get', '', $name);
        $className = Application::$config->get('serviceNamespace') . '\\' . ucfirst($name);

        return class_exists($className) ? $className : null;
    }
}