<?php
namespace hellofresh\bin\Validation;


class ValidationRuleComplex
{
    protected $type;
    protected $attribs = [];
    protected $value;

    const TYPE_VALIDATION_EMAIL = 'email';
    const TYPE_VALIDATION_STRING     = 'string';
    const TYPE_VALIDATION_INT        = 'int';

    /**
     * @param $type
     * @param $attribs
     * @param $value
     */
    public function __construct($type, $attribs, $value)
    {
        $this->type    = $type;
        $this->attribs = $attribs;
        $this->value   = $value;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return array
     */
    public function validate(\Illuminate\Database\Eloquent\Model $model)
    {
        $validate = [];
        switch ($this->type) {
            case ValidationRulefactory::VALIDATION_TYPE_TYPE:
                switch($this->value){
                    case self::TYPE_VALIDATION_EMAIL:
                        foreach ($this->attribs as $attribute) {
                            if (filter_var($model->$attribute, FILTER_VALIDATE_EMAIL) === false) {
                                $validate[] = "Field `{$model->$attribute}` not valid email";
                            }
                        }
                        break;
                }
                break;
        }

        return $validate;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getAttribs()
    {
        return $this->attribs;
    }

}