<?php
namespace hellofresh\bin\Validation;


use hellofresh\bin\entity\iValidationRule;

/**
 * Class ValidationRuleSimple
 *
 * @package hellofresh\bin\Validation
 */
class ValidationRuleSimple implements iValidationRule
{
    protected $type;
    protected $attribs=[];

    /**
     * @param $type
     * @param $attribs
     */
    public function __construct($type, $attribs)
    {
        $this->type    = $type;
        $this->attribs = $attribs;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return array
     */
    public function validate(\Illuminate\Database\Eloquent\Model $model)
    {
        $validate = [];
        switch($this->type){
            case ValidationRulefactory::VALIDATION_TYPE_REQUIRED:
                foreach($this->attribs as $attribute) {
                    if(!$model->$attribute) {
                        $validate[] = "Field `{$attribute}`` not filled";
                    }
                }
                break;
            case ValidationRulefactory::VALIDATION_TYPE_UNIQUE:
                /** @var Model $className */
                $className = get_class($model);
                foreach($this->attribs as $attribute) {
                    $dublicate = $className::where($attribute,$model->$attribute)->where('id', '!=' , $model->id)->first();
                    if($dublicate) {
                        $validate[] = "Field `{$attribute}`` not unique";
                    }
                }
                break;
        }
        return $validate;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getAttribs()
    {
        return $this->attribs;
    }

}