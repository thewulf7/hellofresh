<?php
namespace hellofresh\bin\Validation;


use hellofresh\bin\patterns\MethodFactory;

/**
 * Class ValidationRulefactory
 *
 * @package hellofresh\bin\Validation
 */
class ValidationRulefactory extends MethodFactory
{
    const VALIDATION_TYPE_REQUIRED = 'required';
    const VALIDATION_TYPE_UNIQUE   = 'unique';
    const VALIDATION_TYPE_TYPE     = 'type';
    const VALIDATION_TYPE_DISABLED = 'disabled';

    /**
     * @param $ruleArr
     *
     * @return mixed
     */
    protected function createRule($ruleArr)
    {

        if(count($ruleArr) === 2) {
            $ruleArr[2] = null;
        }

        list($attribs, $type, $value) = $ruleArr;


        switch ($type) {
            case self::VALIDATION_TYPE_REQUIRED:
                $routeRule = new ValidationRuleSimple($type, $attribs);

                return $routeRule;
                break;
                break;
            case self::VALIDATION_TYPE_UNIQUE:
                $routeRule = new ValidationRuleSimple($type, $attribs);

                return $routeRule;
                break;
                break;
            case self::VALIDATION_TYPE_TYPE:
                $routeRule = new ValidationRuleComplex($type, $attribs, $value);

                return $routeRule;
                break;
                break;
            case self::VALIDATION_TYPE_DISABLED:
                $routeRule = new ValidationRuleSimple($type, $attribs);

                return $routeRule;
                break;
            default:
                throw new \InvalidArgumentException("$type is not a valid");
        }
    }

    /**
     * @param $ruleArr
     *
     * @return mixed
     */
    public function create($ruleArr)
    {
        $obj = $this->createRule($ruleArr);

        return $obj;
    }
}