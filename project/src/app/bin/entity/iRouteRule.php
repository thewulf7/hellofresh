<?php
namespace hellofresh\bin\entity;


/**
 * Interface iRouteRule
 *
 * @package hellofresh\bin\entity
 */
interface iRouteRule
{
    /**
     * @return mixed
     */
    public function register();
}