<?php
namespace hellofresh\bin\entity;


/**
 * Interface iValidationRule
 *
 * @package hellofresh\bin\entity
 */
interface iValidationRule
{
    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return mixed
     */
    public function validate(\Illuminate\Database\Eloquent\Model $model);
}