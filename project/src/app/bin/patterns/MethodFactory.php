<?php
namespace hellofresh\bin\patterns;


/**
 * Class MethodFactory
 *
 * @package hellofresh\bin\patterns
 */
abstract class MethodFactory
{
    /**
     * @param $ruleAr
     *
     * @return mixed
     */
    abstract protected function createRule($ruleAr);
}