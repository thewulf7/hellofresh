<?php
namespace hellofresh\config;


/**
 * Interface iConfig
 *
 * @package hellofresh\config
 */
interface iConfig
{
    /**
     * @param $key
     *
     * @return mixed
     */
    public function get($key);
}