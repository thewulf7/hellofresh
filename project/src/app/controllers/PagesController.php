<?php
namespace hellofresh\controllers;


use hellofresh\bin\Controller;

/**
 * Class PagesController
 *
 * @package hellofresh\controllers
 */
class PagesController extends Controller
{
    /**
     * @return array
     */
    public function guestAllowed()
    {
        return array(
            'actionIndexPage',
            'actionLoginPage',
            'actionRegisterPage',
        );
    }

    /**
     * @return \fkooman\Http\JsonResponse
     */
    public function actionIndexPage()
    {
        $data = [
            'title' => 'Index page',
            'data'  => [
                'links' => [
                    [
                        'title' => 'Login page',
                        'link'  => '/#/user/login',
                    ],
                    [
                        'title' => 'Register page',
                        'link'  => '/#/user/register',
                    ],
                    [
                        'title' => 'Search page',
                        'link'  => '/#/user/search',
                    ],
                ],
            ],
        ];

        return $this->sendData($data);
    }

    /**
     * @return \fkooman\Http\JsonResponse
     */
    public function actionLoginPage()
    {
        $data = [
            'title' => 'Login page',
        ];

        return $this->sendData($data);
    }

    /**
     * @return \fkooman\Http\JsonResponse
     */
    public function actionRegisterPage()
    {
        $data = [
            'title' => 'Register page',
        ];

        return $this->sendData($data);
    }

    /**
     * @return \fkooman\Http\JsonResponse
     */
    public function actionSearchPage()
    {
        $data = [
            'title' => 'Search page',
        ];

        return $this->sendData($data);
    }
}