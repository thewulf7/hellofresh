<?php
namespace hellofresh\controllers\rest;


use fkooman\Http\Exception\BadRequestException;
use fkooman\Http\Exception\ForbiddenException;
use fkooman\Http\Request;
use hellofresh\bin\Controller;

/**
 * Class AuthController
 *
 * @package hellofresh\controllers\rest
 */
class AuthController extends Controller
{
    /**
     * @return array
     */
    public function guestAllowed(){
        return array(
            'actionLogin',
            'actionUser'
        );
    }

    /**
     * @param Request $request
     *
     * @return \fkooman\Http\JsonResponse
     */
    public function actionLogin(Request $request)
    {
        $email    = $request->getPostParameter('email');
        $password = $request->getPostParameter('password');

        try {
            $model = $this->getAuthService()->auth($email, $password);
        } catch (ForbiddenException $e) {
            return $this->sendErrorData([$e->getMessage()],$e->getCode());
        } catch(\InvalidArgumentException $e) {
            return $this->sendErrorData(unserialize($e->getMessage()),$e->getCode());
        }

        $user = new \hellofresh\models\frontend\User($model);

        return $this->sendData($user,201);
    }

    /**
     * @param Request $request
     *
     * @return \fkooman\Http\JsonResponse
     */
    public function actionLogout(Request $request){
        $hash = $_SERVER['HTTP_AUTH_KEY'];

        try {
            $this->getAuthService()->logout($hash);
        } catch (ForbiddenException $e) {
            return $this->sendErrorData([$e->getMessage()],$e->getCode());
        }

        return $this->sendEmptyData();
    }

    /**
     * @return \fkooman\Http\JsonResponse
     * @throws ForbiddenException
     */
    public function actionUser()
    {
        $hash = isset($_SERVER['HTTP_AUTH_KEY']) ? $_SERVER['HTTP_AUTH_KEY'] : null;

        if(!$hash) {
            return $this->sendEmptyData();
        }

        try {
            $model = $this->getAuthService()->authCurrentUser($hash);
        } catch(BadRequestException $e) {
            return $this->sendErrorData([$e->getMessage()],$e->getCode());
        }

        $user = new \hellofresh\models\frontend\User($model);

        return $this->sendData($user,201);
    }
}