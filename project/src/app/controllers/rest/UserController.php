<?php
namespace hellofresh\controllers\rest;


use fkooman\Http\Request;
use hellofresh\bin\Controller;
use hellofresh\models\frontend\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    public function guestAllowed()
    {
        return [
            'actionCreate',
        ];
    }

    public function actionCreate(Request $request)
    {
        $email     = $request->getPostParameter('email');
        $password  = $request->getPostParameter('password');
        $rpassword = $request->getPostParameter('rpassword');
        $username  = $request->getPostParameter('username');

        if($rpassword !== $password) {
            return $this->sendErrorData(['Passwords doesn\'t match']);
        }

        try {
            $user = $this->getUserService()->create($email, $password, $username);
        } catch (\InvalidArgumentException $e) {
            return $this->sendErrorData(unserialize($e->getMessage()), $e->getCode());
        }

        $model = new User($user);

        return $this->sendData($model, 201);
    }

    public function actionUpdate($id, Request $request)
    {
        try {
            $user = $this->getUserService()->getUser($id);
        } catch (ModelNotFoundException $e) {
            return $this->sendErrorData([$e->getMessage()]);
        }

        $username = $request->getPostParameter('username');

        try {
            $this->getUserService()->update($user->id, $username);
        } catch (\InvalidArgumentException $e) {
            return $this->sendErrorData(unserialize($e->getMessage()), $e->getCode());
        }

        return $this->sendEmptyData(201);
    }

    public function actionView($id)
    {
        try {
            $user = $this->getUserService()->getUser($id);
        } catch (ModelNotFoundException $e) {
            return $this->sendErrorData([$e->getMessage()]);
        }

        $model = new User($user);

        return $this->sendData($model);
    }

    public function actionDelete($id)
    {
        try {
            $user = $this->getUserService()->getUser($id);
        } catch (ModelNotFoundException $e) {
            return $this->sendErrorData([$e->getMessage()]);
        }

        $this->getUserService()->delete($user->id);

        return $this->sendEmptyData(201);
    }

    public function actionSearch($string)
    {
        $result = $this->getUserService()->search($string);

        $result = array_map(function (\hellofresh\models\backend\User $user) {
            return new User($user);
        }, $result);

        return $this->sendData($result, 200, [User::SERIALIZE_OPTION_WITHOUT => ['userhash']]);
    }
}