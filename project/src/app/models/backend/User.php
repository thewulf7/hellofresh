<?php
namespace hellofresh\models\backend;


use hellofresh\bin\Validation\ValidationRuleComplex;
use hellofresh\bin\Validation\ValidationRulefactory;

/**
 * Class User
 *
 * @package hellofresh\models\backend
 */
class User extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'users';

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array(array('email'),ValidationRulefactory::VALIDATION_TYPE_UNIQUE),
            array(array('email'),ValidationRulefactory::VALIDATION_TYPE_TYPE,ValidationRuleComplex::TYPE_VALIDATION_EMAIL),
            array(array('email','passwd'),ValidationRulefactory::VALIDATION_TYPE_REQUIRED),
            array(array('passwd','salt'),ValidationRulefactory::VALIDATION_TYPE_DISABLED),
        );
    }

    /**
     * @return array
     */
    public function validate()
    {
        $ruleFactory = new ValidationRulefactory();
        $errors = [];

        $rulesAr = $this->rules();

        foreach ($rulesAr as $rule) {
            $rule = $ruleFactory->create($rule);
            if (!in_array($rule->getType(),array(ValidationRulefactory::VALIDATION_TYPE_DISABLED),true)) {
                $errors[] = $rule->validate($this);
            }
        }

        return $errors;
    }

    /**
     * @return bool
     */
    public function safeSave()
    {
        $errors = array_filter($this->validate());
        if (count($errors) === 0) {
            $this->save();
            return true;
        } else {
            throw new \InvalidArgumentException(serialize($errors));
        }
    }
}