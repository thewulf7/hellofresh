<?php
namespace hellofresh\models\frontend;


/**
 * Class User
 *
 * @package hellofresh\models\frontend
 */
class User extends \hellofresh\bin\Model
{

    /**
     * @param \hellofresh\models\backend\User $user
     */
    public function __construct(\hellofresh\models\backend\User $user)
    {
        $this->container = $user;
    }

    /**
     * @param array $options
     *
     * @return mixed
     */
    public function serialize($options=[])
    {
        $data = [
            'id'         => $this->container->id,
            'type'       => $this->modelType(),
            'attributes' => [],
        ];

        $params        = $this->container->toArray();
        $data['attributes'] = $this->getAttributes();

        if(count($options)>0) {
            foreach($options as $type => $params) {
                switch($type){
                    case self::SERIALIZE_OPTION_WITHOUT:
                        foreach($params as $param) {
                            if(array_key_exists($param,$data['attributes'])) {
                                unset($data['attributes'][$param]);
                            }
                        }
                        break;
                }
            }
        }

        return $data;
    }
}