<?php
namespace hellofresh\services;


use fkooman\Http\Exception\ForbiddenException;
use hellofresh\bin\AbstractService;
use hellofresh\models\backend\User;
use hellofresh\bin\Application;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class AuthService
 *
 * @package hellofresh\services
 */
class AuthService extends AbstractService
{
    /**
     * @param $email
     * @param $password
     *
     * @return bool
     * @throws ForbiddenException
     */
    public function auth($email, $password)
    {
        try {
            /** @var User $model */
            $model = User::where('email', $email)->firstOrFail();
        } catch(ModelNotFoundException $e) {
            throw new ForbiddenException("User with email {$email} not found",404);
        }
        $hashpasswd = crypt($password, $model->salt);

        if ($hashpasswd === $model->passwd) {
            $hash            = md5(mt_rand());
            $model->userhash = $hash;
            $model->safeSave();

            Application::setAuth($hash);

            return $model;
        } else {
            throw new ForbiddenException("Wrong password for user {$email}", 403);
        }
    }

    /**
     * @return mixed
     */
    public function getCurrentUser()
    {
        return Application::isGuest();
    }

    /**
     * @param $hash
     *
     * @return mixed
     * @throws ForbiddenException
     */
    public function authCurrentUser($hash){
        try {
            $model = User::where('userhash', $hash)->firstOrFail();
        } catch(ModelNotFoundException $e) {
            throw new ForbiddenException("User with email {$hash} not found",404);
        }

        return $model;
    }

    /**
     * @param $hash
     *
     * @throws ForbiddenException
     */
    public function logout($hash) {
        $model = $this->authCurrentUser($hash);

        Application::clearAuth();
        $model->userhash = null;

        $model->safeSave();
    }
}