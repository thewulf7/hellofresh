<?php
namespace hellofresh\services;


use hellofresh\bin\AbstractService;
use hellofresh\bin\Application;
use hellofresh\models\backend\User;

/**
 * Class UserService
 *
 * @package hellofresh\services
 */
class UserService extends AbstractService
{
    protected $className = 'hellofresh\models\backend\User';

    /**
     * @param $id
     *
     * @return mixed
     */
    public function getUser($id)
    {
        $model = $this->findModel($id);

        return $model;
    }

    /**
     * @param $id
     * @param $username
     */
    public function update($id, $username)
    {
        $model           = $this->findModel($id);
        $model->username = $username;
        $model->safeSave();
    }

    /**
     * @param $email
     * @param $password
     * @param $username
     *
     * @return User
     */
    public function create($email, $password, $username)
    {
        $model           = new User();
        $salt            = Application::generateSalt();
        $model->email    = $email;
        $model->passwd   = Application::generatePasswd($password, $salt);
        $model->salt     = $salt;
        $model->username = $username;

        $model->safeSave();

        return $model;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id)
    {
        $model = $this->findModel($id);

        return $model->delete() ? true : false;
    }

    /**
     * @param $string
     *
     * @return mixed
     */
    public function search($string)
    {
        $models = User::where(function ($query) use ($string) {
            $query->where('username', 'like', $string.'%')
                ->orWhere('email', 'like', $string.'%');
        })->get()->all();

        return $models;
    }
}