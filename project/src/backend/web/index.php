<?php
require(__DIR__ . '/../../../vendor/autoload.php');

$config_data = include(__DIR__ . '/../../config/main.php');
$config      = new \hellofresh\config\Config($config_data);

$application = new \hellofresh\bin\Application($config,new \fkooman\Rest\Service());

$application->run();