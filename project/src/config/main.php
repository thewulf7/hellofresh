<?php
/**
 * Created by PhpStorm.
 * User: johnnyutkin
 * Date: 05.09.15
 * Time: 11:41
 */
return [
    'default'          => [
        'controller' => 'site',
        'action'     => 'index',
    ],
    'serviceNamespace' => 'hellofresh\services',
    'pathToModels'     => [__DIR__ . '/../app/models/backend'],
    'rest'             => [
        'pages' => [
            'class'     => \hellofresh\controllers\PagesController::className(),
            'pluralize' => false,
        ],
        'auth'  => [
            'class'     => \hellofresh\controllers\rest\AuthController::className(),
            'pluralize' => false,
            'rules'     => [
                'login'  => 'POST',
                'logout' => 'POST',
                'user'   => 'GET',
            ],
        ],
        'user'  => \hellofresh\controllers\rest\UserController::className(),
    ],
    'db'               => [
        'dbhost' => '127.0.0.1',
        'dbuser' => 'thewulf7',
        'dbpass' => 'thewulf7_hellof',
        'dbname' => 'thewulf7_hellof',
        'dbport' => '3306',
    ],
];