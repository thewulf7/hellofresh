angular.module('lunchApp', ['ngRoute', 'ngCookies', 'ui.bootstrap'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: './partials/index.html',
                controller: 'indexCtrl'
            }).
            when('/user/login', {
                templateUrl: './partials/login.html',
                controller: 'loginCtrl'
            }).
            when('/user/register', {
                templateUrl: './partials/register.html',
                controller: 'registerCtrl'
            }).
            //when('/user/:placeid', {
            //    templateUrl: './partials/user.html',
            //    controller: 'userCtrl'
            //}).
            when('/user/search', {
                templateUrl: './partials/list.html',
                controller: 'usersCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
    }])
