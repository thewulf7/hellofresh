angular.module('lunchApp')
.controller('indexCtrl', ['$scope','pages', function ($scope,pages) {
    pages.getPage('indexpage').success(function(data){
        $scope.links = data.data.data.links;
    });
}]);