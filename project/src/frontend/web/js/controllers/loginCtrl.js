angular.module('lunchApp')
    .controller('loginCtrl', ['$scope', 'pages', 'api', '$cookies', function ($scope, pages, api, $cookies) {

        $scope.alert = "hidden";
        $scope.form = "hidden";

        if ($cookies.AUTH_KEY) {
            var response = api.getCurrentUser($cookies.AUTH_KEY);

            response.then(function (data) {
                if (data.data.data.id) {
                    $scope.alert = "visible alert-success";
                    $scope.form = "hidden";
                    $scope.success = 'Greetings ' + data.data.data.attributes.username + '! You have been successfully logged in!';

                    $scope.logoutForm = "visible";
                }
            });
        }

        pages.getPage('loginpage').success(function (data) {
            $scope.title = data.data.title;
            $scope.form = "visible";
        });
        $scope.submit = function () {
            var response = api.login($scope.email, $scope.password);

            response.then(function (data) {
                if (data.data.code == 201) {
                    if (data.data.data.id) {
                        $scope.alert = "visible alert-success";
                        $scope.form = "hidden";
                        $scope.logoutForm = "visible";
                        $scope.success = 'Greetings ' + data.data.data.attributes.username + '! You have been successfully logged in!';
                        $cookies.AUTH_KEY = data.data.data.attributes.userhash;
                        $scope.email="";
                        $scope.password="";
                    }
                } else {
                    $scope.alert = "visible alert-danger";
                    $scope.success = data.data.data.errors[0];
                }
            });
        };

        $scope.logout = function () {
            var response = api.logout($cookies.AUTH_KEY);

            response.then(function (data) {
                if (data.status == 200) {
                        $scope.alert = "hidden";
                        $scope.form = "visible";
                        $scope.logoutForm = "hidden";
                        $scope.success = '';
                        $cookies.AUTH_KEY = '';
                }
            });
        };
    }]);