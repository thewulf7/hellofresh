angular.module('lunchApp')
    .controller('registerCtrl', ['$scope', 'pages', 'api', '$location', '$cookies', function ($scope, pages, api,$location, $cookies) {

        $scope.form = "visible";
        $scope.alert = "hidden";
        $scope.message = "";

        if ($cookies.AUTH_KEY) {
            var response = api.getCurrentUser($cookies.AUTH_KEY);

            response.then(function (data) {
                if (data.data.data.id) {
                    $location.path('/user/login');
                }
            });
        }

        pages.getPage('registerpage').success(function (data) {
            $scope.title = data.data.title;
        });

        $scope.submit = function () {
            $scope.alert = "hidden";
            $scope.message = "";
            var response = api.register($scope.email, $scope.password, $scope.rpassword, $scope.username);
            response.then(function (data) {
                console.info(data)
                if(data.data.data.errors) {
                    var errors = data.data.data.errors;
                    for(prop in errors) {
                        $scope.message = errors[prop].join(';\n');
                    }
                    $scope.alert = "visible alert-danger";
                } else {
                    console.info(data.data.data);
                    if(data.data.data.id) {
                        $scope.alert = "visible alert-success";
                        $scope.form = "hidden";
                        $scope.message = 'Greetings ' + data.data.data.attributes.username + '! You have been successfully registered!';
                    }
                }

            });
        };
    }]).directive('passwordMatch', [function () {
        return {
            restrict: 'A',
            scope:true,
            require: 'ngModel',
            link: function (scope, elem , attrs,control) {
                var checker = function () {

                    //get the value of the first password
                    var e1 = scope.$eval(attrs.ngModel);

                    //get the value of the other password
                    var e2 = scope.$eval(attrs.passwordMatch);
                    return e1 == e2;
                };
                scope.$watch(checker, function (n) {

                    //set the form control to valid if both
                    //passwords are the same, else invalid
                    control.$setValidity("unique", n);
                });
            }
        };
    }]);