angular.module('lunchApp')
    .controller('usersCtrl', ['$scope', '$cookies', 'pages', 'api', '$location', function ($scope, $cookies, pages, api,  $location) {

        if (!$cookies.AUTH_KEY || $cookies.AUTH_KEY === '') {
            $location.path('/user/login');
        }

        pages.getPage('searchpage').success(function (data) {
            $scope.title = data.data.title;
            $scope.form = "visible";
        });

        $scope.submit = function () {
            api.search($scope.filterText,$cookies.AUTH_KEY).then(function (data) {
                if (data.data.data) {
                    $scope.users = data.data.data;
                    console.info(data);
                }
            });
        }
    }]);