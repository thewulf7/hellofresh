angular.module('lunchApp').factory('api', ['$http', function ($http) {

    return {
        login: function (email, password) {
            return $http({
                url: "http://api.app.dev:8080/v1/auth/login",
                method: "POST",
                data: 'email=' + email + '&password=' + password,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            });
        },
        logout: function (hash) {
            return $http({
                url: "http://api.app.dev:8080/v1/auth/logout",
                method: "POST",
                data: '',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Auth-Key': hash,
                }
            });
        },
        getCurrentUser: function (hash) {
            return $http({
                url: "http://api.app.dev:8080/v1/auth/user",
                method: "GET",
                data: '',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Auth-Key': hash
                }
            });
        },
        register: function (email, password, rpassword, username) {
            return $http({
                url: "http://api.app.dev:8080/v1/users",
                method: "POST",
                data: 'email=' + email + '&password=' + password + '&rpassword=' + rpassword + '&username=' + username,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }
            });
        },
        search: function(string,hash){
            return $http({
                url: "http://api.app.dev:8080/v1/users/search/"+string,
                method: "POST",
                data: '',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Auth-Key': hash
                }
            });
        }
    };

}]);