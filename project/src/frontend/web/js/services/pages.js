angular.module('lunchApp').factory('pages', ['$http',function ($http) {
	
	return {
		getPage : function(url){
			return $http.get("http://api.app.dev:8080/v1/pages/" + url);
		},

	};

}]);